# Calendar Management

DISCLAIMER: these are high level requirements that the team can adjust and enhance as they see fit.

Calendar management should serve two purposes; helping financial services to determine working days and 
non-working days, and helping corporate users to keep track of important business events. 

### Holidays and Weekends
Although there should be APIs to enable bank admin to define holidays and configure weekends, currently 
no UI needs to be implemented for this, as these will be synchronized from PS-PayHub (for OAB case).

We still need to check how to sync. holidays and weekends from PS-PayHub, but **optimally** we will call 
APIs in PS-PayHub to get the defined holidays and PS-PayHub will push to us any updates on the holidays.

##### APIs for CORPay services
* isWorkingDay: receives a specific date and returns whether it is a working day or not
* getNextWorkingDay: calculates the next working day starting from today
* getNextWorkingDayAfterXDays: calculates the next working day starting from today and adding X working days

### Reminders
CORPay services publishes important business events as JMS messages, some of these events has financial 
implications in a future date, we need corporate? users to be able to configure multiple reminders for 
each of these business events so that corporate users are notified about them.

To send reminders to users on the configured time, you only need to send a [CORPay event](https://progressoft.gitlab.io/kryptonite/docs/#/architecture/event-conventions) and the notification service 
will handle sending this event to the users via their preferred channel.

##### Example 
When a corporate applies for a Letter of Credit (LC) on the system and once the application is approved, 
the LC will be activated, and a CORPay event called `LC_ACTIVATED` will be published, this event contains  
the LC details including its `expiryDate` which is the date this LC will be automatically closed unless 
the corporate requested to extend it.

As a corporate user I should be able to configure reminders on the system as the following examples:
* 3 weeks before LC expiry at 2:00 PM
* again 3 days before LC expiry at 2:00 PM
* again on the expiry date at 8:00 AM

If the corporate extends the LC, all reminder dates should be updated to match the user configuration above and if the 
LC is closed before the expiry, all reminders should be deleted.

### Calendar Widget
The corporate users should have a view of the working days, holidays and future reminders on the system home page as a 
calendar widget. 
* Weekends and Holidays should be highlighted to distinguish between them and the working days
* Dates with important business events should be marked (the LC expiry date itself, not the configured reminder dates)
* The user should be able to click on the marked event dates and view the details on the event and the configured reminders on it. 